package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Tarif;
import com.musaida.tchamia.repositories.TarifRepository;
import com.musaida.tchamia.services.interfaces.TarifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class TarifServiceImpl implements TarifService {

    @Autowired
    TarifRepository tarifRepository;

    @Override
    public List<Tarif> getAllTarif() {
        return tarifRepository.findAll();

    }

    @Override
    public Tarif getOneTarif(Long id) {
        return tarifRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(TARIF, ID, id));
    }

    @Override
    public Tarif addOneTarif(Tarif tarif) {
        Tarif tarif1=new Tarif();
        tarif1.setTypeColis(tarif.getTypeColis());
        tarif1.setMontant(tarif.getMontant());
        return tarifRepository.save(tarif);
    }

    @Override
    public ApiResponse deleteTarif(Long id) {
        tarifRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(TARIF, ID, id)
        );
        tarifRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");


    }

    @Override
    public Tarif updateTarif(Long id, Tarif tarif) {
        Tarif tarif2 = tarifRepository.findById(tarif.getId()).orElseThrow(
                () -> new ResourceNotFoundException(TARIF,ID,id ));

        tarif2.setTypeColis(tarif.getTypeColis());
        tarif2.setMontant(tarif.getMontant());


        return tarifRepository.save(tarif);
    }

}



