package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Avis;
import com.musaida.tchamia.repositories.AvisRepository;
import com.musaida.tchamia.services.interfaces.AvisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class AvisServiceImpl implements AvisService {

    @Autowired
    AvisRepository avisRepository;

    @Override
    public List<Avis> getAllAvis() {
        return avisRepository.findAll();
    }

    @Override
    public Avis getOneAvis(Long id) {
        return avisRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(AVIS,ID,id)
        );
    }

    @Override
    public Avis addOneAvis(Avis avis) {
        Avis avis1= new Avis();
        avis1.setNotation(avis.getNotation());
        avis1.setAvis(avis.getAvis());
        return avisRepository.save(avis);
    }

    @Override
    public ApiResponse deleteAvis(Long id) {
        avisRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(AVIS, ID, id)
        );
        avisRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }

    @Override
    public Avis updateAvis(Long id, Avis avis) {
        Avis avis2 = avisRepository.findById(avis.getId()).orElseThrow(
                () -> new ResourceNotFoundException(AVIS,ID,id ));

        avis2.setNotation(avis.getNotation());
        avis2.setAvis(avis.getAvis());
        return avisRepository.save(avis2);
    }
}

