package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Point;
import com.musaida.tchamia.repositories.PointRepository;
import com.musaida.tchamia.services.interfaces.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;


@Service

public class PointServiceImpl implements PointService {

    @Autowired
    PointRepository pointRepository;
    @Override
    public List<Point> getAllPoint() {
        return pointRepository.findAll() ;
    }

    @Override
    public Point getOnePoint(Long id) {
        return pointRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(POINT,ID,id)
        );
    }

    @Override
    public Point addOnePoint(Point point) {
        Point point1=new Point();
        point1.setCodePoint(point.getCodePoint());
        point1.setLatitude(point.getLatitude());
        point1.setLongitude(point.getLongitude());
        point1.setNom(point.getNom());
        point1.setTel(point.getTel());
        point1.setUser(point.getUser());
        return pointRepository.save(point);
    }

    @Override
    public ApiResponse deletePoint(Long id) {
        pointRepository.findById(id).orElseThrow(
                ()->  new ResourceNotFoundException(POINT, ID,id)
        );
        pointRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"delete") ;
    }

    @Override
    public Point updatePoint(Long id, Point point) {
        Point Point2 = pointRepository.findById(point.getId()).orElseThrow(
                () -> new ResourceNotFoundException(POINT,ID,id ));

        Point2.setCodePoint(point.getCodePoint());
        Point2.setLatitude(point.getLatitude());
        Point2.setLongitude(point.getLongitude());
        Point2.setNom(point.getNom());
        Point2.setTel(point.getTel());
        Point2.setUser(point.getUser());


        return pointRepository.save(point);
    }
}


