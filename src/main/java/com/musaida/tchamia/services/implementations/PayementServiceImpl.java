package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Payement;
import com.musaida.tchamia.repositories.PayementRepository;
import com.musaida.tchamia.services.interfaces.PayementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service

public class PayementServiceImpl implements PayementService {

    @Autowired
    PayementRepository payementRepository;

    @Override
    public List<Payement> getAllPayements() {
        return payementRepository.findAll();
    }

    @Override
    public Payement getOnePayement(Long id) {
        return payementRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(PAYEMENT,ID, id)
        );
    }

    @Override
    public Payement addOnePayement(Payement payement) {
        Payement payement1= new Payement();
        payement1.setMontant(payement.getMontant());
        return payementRepository.save(payement);

    }

    @Override
    public ApiResponse deletePayement(Long id) {
        payementRepository.findById(id).orElseThrow(
                ()->  new ResourceNotFoundException(PAYEMENT, ID,id)
        );
        payementRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"delete") ;
    }

    @Override
    public Payement updatePayement(Long id, Payement payement) {
        Payement payement2 = payementRepository.findById(payement.getId()).orElseThrow(
                () -> new ResourceNotFoundException(PAYEMENT,ID,id ));

        payement2.setMontant(payement.getMontant());
        return payementRepository.save(payement);
    }


}

