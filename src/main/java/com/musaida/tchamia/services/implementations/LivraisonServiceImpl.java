package com.musaida.tchamia.services.implementations;


import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Category;
import com.musaida.tchamia.models.Livraison;
import com.musaida.tchamia.repositories.LivraisonRepository;
import com.musaida.tchamia.services.interfaces.LivraisonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class LivraisonServiceImpl implements LivraisonService {

    @Autowired
    LivraisonRepository livraisonRepository;


    @Override
    public List<Livraison> getAllLivraison() {

        return livraisonRepository.findAll();
    }

    @Override
    public Livraison getOneLivraison(Long id) {

        return livraisonRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(LIVRAISON,ID,id)
        );
    }

    @Override
    public Livraison addOneLivraison(Livraison livraison) {
        Livraison livraison1= new Livraison();
        livraison1.setCodeLivraison(livraison.getCodeLivraison());
        livraison1.setLatitude(livraison.getLatitude());
        livraison1.setLongitude(livraison.getLongitude());
        return livraisonRepository.save(livraison);
    }

    @Override
    public ApiResponse deleteLivraison(Long id) {
        livraisonRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(LIVRAISON, ID, id)
        );
        livraisonRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }


    @Override
    public Livraison updateLivraison(Long id, Livraison livraison) {
        Livraison livraison2     = livraisonRepository.findById(livraison.getId()).orElseThrow(
                () -> new ResourceNotFoundException(LIVRAISON,ID,id ));

        livraison2.setCodeLivraison(livraison.getCodeLivraison());
        livraison2.setLatitude(livraison.getLatitude());
        livraison2.setLongitude(livraison.getLongitude());
        return livraisonRepository.save(livraison);
    }
}
