package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Lot;
import com.musaida.tchamia.repositories.LotRepository;
import com.musaida.tchamia.services.interfaces.LotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.ID;
import static com.musaida.tchamia.utils.AppConstant.LOT;
import static com.musaida.tchamia.utils.AppConstant.POINT;



@Service
public class LotServiceImpl implements LotService {

    @Autowired
    LotRepository lotRepository;

    @Override
    public List<Lot> getAllLot() {
        return lotRepository.findAll();
    }

    @Override
    public Lot getOneLot(Long id) {
        return lotRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(LOT,ID, id)
        );
    }

    @Override
    public Lot addOneLot(Lot lot) {
        Lot lot1= new Lot();
        lot1.setLatitude(lot.getLatitude());
        lot1.setLongitude(lot.getLongitude());
        lot1.setCode(lot.getCode());

        return lotRepository.save(lot);
    }

    @Override
    public ApiResponse deleteLot(Long id) {
        lotRepository.findById(id).orElseThrow(
                ()->  new ResourceNotFoundException(LOT, ID,id)
        );
        lotRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"delete") ;
    }

    @Override
    public Lot updateLot(Long id, Lot lot) {
        Lot lot2 = lotRepository.findById(lot.getId()).orElseThrow(
                () -> new ResourceNotFoundException(POINT,ID,id ));

        lot2.setLatitude(lot.getLatitude());
        lot2.setLongitude(lot.getLongitude());
        lot2.setCode(lot.getCode());


        return lotRepository.save(lot);
    }

}
