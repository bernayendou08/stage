package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.TypeService;
import com.musaida.tchamia.repositories.TypeServiceRepository;
import com.musaida.tchamia.services.interfaces.TypeServiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceServiceImpl implements TypeServiceService {

    @Autowired
   TypeServiceRepository typeServiceRepository;

    @Override
    public List<TypeService> getAllTypeService() {
        return null;
    }

    @Override
    public TypeService getOneTypeService(Long id) {
        return null;
    }

    @Override
    public TypeService addOneTypeService(TypeService typeService) {
        return null;
    }

    @Override
    public ApiResponse deleteTypeService(Long id) {
        return null;
    }

    @Override
    public TypeService updateTypeDeService(Long id, TypeService typeDeService) {
        return null;
    }
}
