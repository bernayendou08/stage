package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Engin;
import com.musaida.tchamia.repositories.EnginRepository;
import com.musaida.tchamia.services.interfaces.EnginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class EnginServiceImpl implements EnginService {

    @Autowired
    EnginRepository enginRepository;

    @Override
    public List<Engin> getAllEngin() {
        return enginRepository.findAll();
    }

    @Override
    public Engin getOneEngin(Long id) {
        return enginRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(ENGIN,ID,id)
        );
    }

    @Override
    public Engin addOneEngin(Engin engin) {
        Engin engin1= new Engin();
        engin1.setTypeEngin(engin.getTypeEngin());
        engin1.setLibele(engin.getLibele());
        engin1.setDescription(engin.getDescription());
        engin1.setTypeEngin(engin.getTypeEngin());
        engin1.setMatricule(engin1.getMatricule());
        return enginRepository.save(engin);
    }

    @Override
    public ApiResponse deleteEngin(Long id) {
        enginRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(ENGIN, ID, id)
        );
        enginRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }

    @Override
    public Engin updateEngin(Long id, Engin engin) {
        Engin engin2 = enginRepository.findById(engin.getId()).orElseThrow(
                () -> new ResourceNotFoundException(ENGIN,ID,id ));

        engin2.setTypeEngin(engin.getTypeEngin());
        engin2.setLibele(engin.getLibele());
        engin2.setDescription(engin.getDescription());
        engin2.setTypeEngin(engin.getTypeEngin());
        engin2.setMatricule(engin.getMatricule());

        return enginRepository.save(engin);
    }
}
