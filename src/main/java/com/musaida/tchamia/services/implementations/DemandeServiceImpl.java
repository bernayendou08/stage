package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Demande;
import com.musaida.tchamia.repositories.DemandeRepository;
import com.musaida.tchamia.services.interfaces.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class DemandeServiceImpl implements DemandeService {

    @Autowired
    DemandeRepository demandeRepository;

    @Override
    public List<Demande> getAllDemande() {
        return demandeRepository.findAll();
    }

    @Override
    public Demande getOneDemande(Long id) {
        return demandeRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(DEMANDE,ID,id));
    }

    @Override
    public Demande addOneDemande(Demande demande) {
        Demande demande1 = new Demande();
        demande1.setLibele(demande1.getLibele());
        demande1.setAdressDeLivrason(demande.getAdressDeLivrason());
        demande1.setDateDeLivraison(demande.getDateDeLivraison());
        demande1.setFuullNameRecepteur(demande.getFuullNameRecepteur());
        demande1.setQuantiter(demande.getQuantiter());
        return demandeRepository.save(demande);
    }

    @Override
    public ApiResponse deleteDemande(Long id) {
        demandeRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(DEMANDE, ID, id)
        );
       demandeRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }

    @Override
    public Demande updateDemande(Long id, Demande demande) {
       Demande existingdemande = demandeRepository.findById(demande.getId()).orElseThrow(
                () -> new ResourceNotFoundException(DEMANDE,ID,id ));

        existingdemande.setLibele(demande.getLibele());
        return demandeRepository.save(existingdemande);
    }
}
