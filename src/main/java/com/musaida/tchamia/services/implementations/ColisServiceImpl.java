package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Category;
import com.musaida.tchamia.models.Colis;
import com.musaida.tchamia.models.Point;
import com.musaida.tchamia.repositories.ColisRepository;
import com.musaida.tchamia.services.interfaces.ColisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class ColisServiceImpl implements ColisService {

    @Autowired
    ColisRepository colisRepository;
    @Override
    public List<Colis> getAllColis() {
        return colisRepository.findAll();
    }

    @Override
    public Colis getOneColis(Long id) {
        return colisRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(COLIS,ID,id)
        );
    }

    @Override
    public Colis addOneColis(Colis colis) {
        Colis colis1=new Colis();
        colis1.setVarchar(colis.getVarchar());
        colis1.setLatitude(colis.getLatitude());
        colis1.setLongitude(colis.getLongitude());
        colis1.setCode(colis.getCode());

        return colisRepository.save(colis);

    }

    @Override
    public ApiResponse deleteColis(Long id) {
        colisRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(COLIS, ID, id)
        );
        colisRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");

    }

    @Override
    public Colis updateColis(Long id, Colis colis) {
       Colis existingcolis1 = colisRepository.findById(colis.getId()).orElseThrow(
                () -> new ResourceNotFoundException(COLIS,ID,id ));

        existingcolis1.setVarchar(colis.getVarchar());
        existingcolis1.setLatitude(colis.getLatitude());
        existingcolis1.setLongitude(colis.getLongitude());
        existingcolis1.setCode(colis.getCode());
        return colisRepository.save(existingcolis1);
    }
}
