package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Distance;
import com.musaida.tchamia.repositories.DistanceRepository;
import com.musaida.tchamia.services.interfaces.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class DistanceServiceImpl implements DistanceService {
    @Autowired
    DistanceRepository distanceRepository;

    @Override
    public List<Distance> getAllDistance() {
        return distanceRepository.findAll();
    }

    @Override
    public Distance getOneDistance(Long id) {
        return distanceRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(DISTANCE,ID,id));
    }

    @Override
    public Distance addOneDistance(Distance distance) {
        Distance distance1 = new Distance();
        distance1.setCalcule(distance.getCalcule());

        return distanceRepository.save(distance);

    }

    @Override
    public ApiResponse deleteDistance(Long id) {
        distanceRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(DISTANCE, ID, id)
        );
        distanceRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }

    @Override
    public Distance updateDistance(Long id, Distance distance) {

        Distance distance2 = distanceRepository.findById(distance.getId()).orElseThrow(
                () -> new ResourceNotFoundException(DISTANCE,ID,id ));

        distance2.setCalcule(distance.getCalcule());
        return distanceRepository.save(distance);
    }

}
