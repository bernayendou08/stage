package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Category;
import com.musaida.tchamia.repositories.CategoryRepository;
import com.musaida.tchamia.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.CATEGORY;
import static com.musaida.tchamia.utils.AppConstant.ID;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getOneCategory(Long idCategory) {
        return categoryRepository.findById(idCategory).orElseThrow(
                () ->new ResourceNotFoundException(CATEGORY, ID, idCategory));
    }

    @Override
    public Category addOneCategory(Category category) {
        Category category1=new Category();
        category1.setLibele(category.getLibele());
        return categoryRepository.save(category);
    }

    @Override
    public ApiResponse deleteCategory(Long idCategory) {
        //verify resource Id before delete, if not exciste trow exception
        //or delete quick
         categoryRepository.findById(idCategory).orElseThrow(
                 () ->new ResourceNotFoundException(CATEGORY, ID, idCategory)
         );
         categoryRepository.deleteById(idCategory);
         return new ApiResponse(Boolean.TRUE,"Delete Successfully");
    }

    @Override
    public Category updateCategory(Long id, Category category) {
        Category existingCategory = categoryRepository.findById(category.getId()).orElseThrow(
                () -> new ResourceNotFoundException(CATEGORY,ID,id ));

        existingCategory.setLibele(category.getLibele());
        return categoryRepository.save(existingCategory);
    }
}
