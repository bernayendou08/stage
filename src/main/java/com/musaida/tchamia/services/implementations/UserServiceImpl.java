package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.request.UpdateGeoData;
import com.musaida.tchamia.dtos.request.UpdateProfileDto;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.TrackDetail;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.User;
import com.musaida.tchamia.repositories.UserRepository;
import com.musaida.tchamia.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;



    @Override
    public ApiResponse updateUserProfil(long userId, UpdateProfileDto updateProfileDto) {
        User user1 = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException(USER,ID,userId ));
        user1.setFullname(updateProfileDto.getFullname());
        return new ApiResponse(Boolean.TRUE,"update user profil");
    }

    @Override
    public ApiResponse updatePassword(long userId, String password) {
        User user1 = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException(USER,ID,userId ));
        user1.setPassword(user1.getPassword());

        return new ApiResponse(Boolean.TRUE,"update password");
    }

    @Override
    public ApiResponse forgotPassword(String email) {
       if (! userRepository.existsByEmail(email)){
           throw new ResourceNotFoundException(USER,EMAIL,email);
       }

        return new ApiResponse(Boolean.TRUE,"save successfully");
    }

    @Override
    public List<User> getAllUserInDB() {
        return userRepository.findAll();
    }

/*    @Override
    public List<User> getAllUserByRole(long roleId) {
        return userRepository.findByRole(roleId);

    }*/

    @Override
    public ApiResponse updateUserSate(long userId) {
        User user1 = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException(USER,ID,userId ));

        user1.setActive(!user1.isActive());
        userRepository.save(user1);
        return new ApiResponse(Boolean.TRUE,"save successfully");
    }

    @Override
    public ApiResponse updateGeoData(long userId, UpdateGeoData updateGeoData) {
        User user=userRepository.findById(userId).orElseThrow(
                ()-> new ResourceNotFoundException(USER,ID,userId));
        user.setLat(updateGeoData.getLatitude());
        user.setLng(updateGeoData.getLongitude());
        userRepository.save(user);
        return new ApiResponse(Boolean.TRUE, " geolocalisation update successfully");
    }

    @Override
    public TrackDetail getTrack(long userId) {
        User trackDetail=userRepository.findById(userId).orElseThrow(
                ()-> new ResourceNotFoundException(USER,ID,userId));
        return null;
    }



}





