package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Type;
import com.musaida.tchamia.repositories.TypeRepository;
import com.musaida.tchamia.services.interfaces.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

import static com.musaida.tchamia.utils.AppConstant.*;

@Service
public class TypeServiceimpl implements TypeService {
    @Autowired
    TypeRepository typeRepository;

    @Override
    public List<Type> getAllType() {
        return typeRepository.findAll();
    }

    @Override
    public Type getOneType(Long id) {
        return typeRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(TYPE, ID, id));
    }

    @Override
    public Type addOneType(Type type) {
        Type type1=new Type();
        type1.setDescription(type.getDescription());
        type1.setPrix(type.getPrix());
        type1.setSize(type.getSize());
        return typeRepository.save(type);
    }


    @Override
    public ApiResponse deleteType(Long id) {
        //verify resource Id before delete, if not exciste trow exception
        //or delete quick
        typeRepository.findById(id).orElseThrow(
                () ->new ResourceNotFoundException(TYPE, ID, id)
        );
        typeRepository.deleteById(id);
        return new ApiResponse(Boolean.TRUE,"Delete Successfully");


    }

    @Override
    public Type updateType(Long id, Type type) {
        Type existingType = typeRepository.findById(type.getId()).orElseThrow(
                () -> new ResourceNotFoundException(TYPE,ID,id ));

        existingType.setSize(type.getSize());
        existingType.setDescription(type.getDescription());
        existingType.setPrix(type.getPrix());

        return typeRepository.save(existingType);
    }
}

