package com.musaida.tchamia.services.implementations;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Etat;
import com.musaida.tchamia.repositories.EtatRepository;
import com.musaida.tchamia.services.interfaces.EtatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.musaida.tchamia.utils.AppConstant.ETAT;
import static com.musaida.tchamia.utils.AppConstant.ID;

@Service

public class EtatServiceImpl implements EtatService {

        @Autowired
        EtatRepository etatRepository;


    @Override
    public List<Etat> getAllEtat() {
        return etatRepository.findAll();
    }

    @Override
    public Etat getOneEtat(Long id) {
        return etatRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(ETAT, ID, id)
        );
    }

    @Override
    public Etat addOneEtat(Etat etat) {
        return etatRepository.save(etat);
    }

    @Override
    public ApiResponse deleteEtat(Long id) {
         Etat etatToDelete = etatRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(ETAT,ID,id)
        );
         etatRepository.delete(etatToDelete);
         return  new ApiResponse(Boolean.TRUE,"etat delete successfully");
    }


  /*  @Override
    public Etat updateEtat(Long id, Etat etat) {
        Optional<Etat> etatToUpdate = etatRepository.findById(id);
        if (etatToUpdate.isPresent()){
            //persistance not a optional resource
            Etat etatPersist = etatToUpdate.get();

            //start update
            etatPersist.setStatut(etat.getStatut());
            return etatRepository.save(etatPersist);
        }
        else {
          throw new ResourceNotFoundException(E)  ;
        }
    }*/
    @Override
    public Etat updateEtat(Long id, Etat etat){
        Etat etat1= etatRepository.findById(id).orElseThrow(
                ()-> new ResourceNotFoundException(ETAT, ID ,id)
        );
        etat1.setStatut(etat.getStatut());

        return etatRepository.save(etat) ;
    }
}
