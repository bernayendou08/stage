package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Engin;

import java.util.List;

public interface EnginService {
    //Liste de tout les engin
    List<Engin> getAllEngin();

    //Prendre un Engin
   Engin getOneEngin(Long id);

    //Ajouter enjin
    Engin addOneEngin(Engin engin);

    // suprimer un Engin
    ApiResponse deleteEngin(Long id);

    //modifier un engin
    Engin updateEngin(Long id,Engin engin);

}
