package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Type;

import java.util.List;

public interface TypeService {

        //Liste de tout les type
        List<Type> getAllType();

        //Prendre un type
        Type getOneType(Long id);

        //Ajouter un type
        Type addOneType(Type type);

        // suprimer un type
        ApiResponse deleteType(Long id);

        //modifier un type
        Type updateType(Long id, Type type);
}
