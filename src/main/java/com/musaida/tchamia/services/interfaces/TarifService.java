package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Tarif;

import java.util.List;

public interface TarifService {
    //Liste de tout les tarif
    List<Tarif> getAllTarif();

    //Prendre un tarif
    Tarif getOneTarif(Long id);

    //Ajouter un tarif
    Tarif addOneTarif(Tarif tarif);

    // suprimer un tarif
    ApiResponse deleteTarif(Long id);

    //modifier un tarif
    Tarif updateTarif(Long id, Tarif tarif);

}

