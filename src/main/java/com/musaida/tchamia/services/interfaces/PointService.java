package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Point;

import java.util.List;

public interface PointService {

    //Liste de tout les point
    List<Point> getAllPoint();

    //Prendre un point
    Point getOnePoint(Long id);

    //Ajouter point
    Point addOnePoint(Point point);

    // suprimer un point
    ApiResponse deletePoint(Long id);

    //modifier un point
    Point updatePoint(Long id, Point point);

}
