package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Category;

import java.util.List;

public interface CategoryService {

    //Liste de tout les categories
    List<Category> getAllCategory();

    //Prendre un categorie
    Category getOneCategory(Long idCategory);

    //Ajouter categorie
    Category addOneCategory(Category category);

    // suprimer un categories
    ApiResponse deleteCategory(Long idCategory);

    //modifier un categories
    Category updateCategory(Long id, Category category);


}
