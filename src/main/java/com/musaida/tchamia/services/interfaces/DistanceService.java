package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Distance;

import java.util.List;

public interface DistanceService {

    //Liste de tout les Distance
    List<Distance> getAllDistance();

    //Prendre un Demande
    Distance getOneDistance(Long id);

    //Ajouter Distance
    Distance addOneDistance(Distance distance);

    // suprimer une Distance
    ApiResponse deleteDistance(Long id);

    //modifier un Distance
    Distance updateDistance(Long id, Distance distance);


}
