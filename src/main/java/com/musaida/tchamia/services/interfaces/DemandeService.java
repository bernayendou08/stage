package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Demande;

import java.util.List;

public interface DemandeService {

    //Liste de tout les Demandes
    List<Demande> getAllDemande();

    //Prendre un Demande
    Demande getOneDemande(Long id);

    //Ajouter Demande
    Demande addOneDemande(Demande demande);

    // suprimer un Demande
    ApiResponse deleteDemande(Long id);

    //modifier un Demande
    Demande updateDemande(Long id, Demande demande);

}
