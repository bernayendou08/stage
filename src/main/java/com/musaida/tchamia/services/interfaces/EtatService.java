package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Etat;

import java.util.List;
import java.util.Optional;

public interface EtatService {
    //Liste de tout les Etat
    List<Etat> getAllEtat();

    //Prendre un Etat
    Etat getOneEtat(Long id);

    //Ajouter Etat
    Etat addOneEtat(Etat etat);

    // suprimer un Etat
    ApiResponse deleteEtat(Long id);

    //modifier un Etat
    Etat updateEtat(Long id, Etat etat);


}

