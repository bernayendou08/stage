package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Avis;

import java.util.List;

public interface AvisService {
    //Liste de tout les Avis
    List<Avis> getAllAvis();

    //Prendre un Avis
    Avis getOneAvis(Long id);

    //Ajouter Avis
    Avis addOneAvis(Avis avis);

    // suprimer un Avis
    ApiResponse deleteAvis(Long id);

    //modifier un avis
    Avis updateAvis(Long id, Avis avis);


}
