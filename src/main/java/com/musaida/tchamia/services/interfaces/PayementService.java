package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Payement;


import java.util.List;

public interface PayementService {
    //Liste de tout les payement;
    List<Payement> getAllPayements();

    //Prendre un payement
    Payement getOnePayement(Long id);

    //Ajouter payement
    Payement addOnePayement(Payement payement);

    // suprimer un payement
    ApiResponse deletePayement(Long id);

    //modifier un payement
    Payement updatePayement(Long id, Payement payement);

}

