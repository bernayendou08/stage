package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.TypeService;

import java.util.List;

public interface TypeServiceService {
    //Liste de tout les typeservice
    List<TypeService> getAllTypeService();

    //Prendre un TypeDeService
    TypeService getOneTypeService(Long id);

    //Ajouter TypeService
    TypeService addOneTypeService(TypeService typeService);

    // suprimer un TypeService
    ApiResponse deleteTypeService(Long id);

    //modifier un typedeservice
    TypeService updateTypeDeService(Long id, TypeService typeDeService);
}
