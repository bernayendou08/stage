package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Colis;

import java.util.List;

public interface ColisService {
    //Liste de tout les colis
    List<Colis> getAllColis();

    //Prendre un Colis
    Colis getOneColis(Long id);

    //Ajouter Colis
    Colis addOneColis(Colis colis);

    // suprimer un colis
    ApiResponse deleteColis(Long id);

    //modifier un colis
    Colis updateColis(Long id, Colis colis);


}
