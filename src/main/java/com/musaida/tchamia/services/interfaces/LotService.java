package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Lot;

import java.util.List;

public interface LotService {
    //Liste de tout les lot
    List<Lot> getAllLot();

    //Prendre un lot
   Lot getOneLot(Long id);

    //Ajouter lot
    Lot addOneLot(Lot lot);

    // suprimer un lot
    ApiResponse deleteLot(Long id);

    //modifier un lot
    Lot updateLot(Long id, Lot lot);

}
