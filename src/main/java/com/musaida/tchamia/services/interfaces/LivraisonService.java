package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Livraison;

import java.util.List;

public interface LivraisonService {
    //Liste de tout les Livraisons
    List<Livraison> getAllLivraison();

    //Prendre un Livraison
    Livraison getOneLivraison(Long id);

    //Ajouter Livraison
    Livraison addOneLivraison(Livraison livraison);

    // suprimer un Livraison
    ApiResponse deleteLivraison(Long id);

    //modifier un livraison
    Livraison updateLivraison(Long id, Livraison livraison);

}
