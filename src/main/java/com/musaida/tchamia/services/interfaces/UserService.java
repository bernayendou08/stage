package com.musaida.tchamia.services.interfaces;

import com.musaida.tchamia.dtos.request.UpdateGeoData;
import com.musaida.tchamia.dtos.request.UpdateProfileDto;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.TrackDetail;
import com.musaida.tchamia.models.User;

import java.util.List;

public interface UserService {

    ApiResponse updateUserProfil (long userId, UpdateProfileDto updateProfileDto);
    ApiResponse updatePassword(long userId,  String password);

    ApiResponse forgotPassword (String email);

    List<User> getAllUserInDB();

//    List<User> getAllUserByRole(long roleId);

    ApiResponse updateUserSate(long userId);

    ApiResponse updateGeoData(long userId, UpdateGeoData updateGeoData);

    TrackDetail getTrack(long userId);






}
