package com.musaida.tchamia.services;

import com.musaida.tchamia.dtos.request.SignupResquest;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.models.Geo;
import com.musaida.tchamia.models.User;
import com.musaida.tchamia.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return UserDetailImpl.build(user);
}




}

