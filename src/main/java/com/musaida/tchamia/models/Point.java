package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Points")
public class Point {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Nom;
    private String Tel;
    private String Longitude;
    private String Latitude;
    private String CodePoint;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    public Point() {
    }

    public Point(String nom, String tel, String longitude, String latitude, String codePoint) {
        Nom = nom;
        Tel = tel;
        Longitude = longitude;
        Latitude = latitude;
        CodePoint = codePoint;
    }

    public Point(Long id, String nom, String tel, String longitude, String latitude, String codePoint) {
        this.id = id;
        Nom = nom;
        Tel = tel;
        Longitude = longitude;
        Latitude = latitude;
        CodePoint = codePoint;
    }

    public String getCodePoint() {
        return CodePoint;
    }

    public void setCodePoint(String codePoint) {
        CodePoint = codePoint;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public Date getUpdateat() {
        return UpdatedAt;
    }

    public void setUpdateat(Date updateat) {
        UpdatedAt = updateat;
    }

    public Date getCreatedat() {
        return CreatedAt;
    }

    public void setCreatedat(Date createdat) {
        CreatedAt = createdat;
    }

    public Long getId() {
        return id;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public void setId(Long id) {
        this.id = id;
    }



}
