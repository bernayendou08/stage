package com.musaida.tchamia.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "Payements")

public class Payement {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private double Montant;

    public Payement() {
    }

    public Payement(double montant) {
        Montant = montant;
    }

    public Payement(Long id, double montant) {
        this.id = id;
        Montant = montant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getMontant() {
        return Montant;
    }

    public void setMontant(double montant) {
        Montant = montant;
    }
}
