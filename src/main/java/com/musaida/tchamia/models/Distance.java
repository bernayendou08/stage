package com.musaida.tchamia.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TarifDistants")
public class Distance {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int Calcule;

    public Distance() {
    }

    public Distance(int calcule) {
        Calcule = calcule;
    }

    public Distance(Long id, int calcule) {
        this.id = id;
        Calcule = calcule;

        }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getCalcule() {
        return Calcule;
    }

    public void setCalcule(int calcule) {
        Calcule = calcule;
    }
}
