package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Lot")



public class Lot {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int Code;
    private String Longitude;
    private String Latitude;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Lot() {
    }

    public Lot(int code, String longitude, String latitude) {
        Code = code;
        Longitude = longitude;
        Latitude = latitude;
    }

    public Lot(Long id, int code, String longitude, String latitude) {
        this.id = id;
        Code = code;
        Longitude = longitude;
        Latitude = latitude;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
