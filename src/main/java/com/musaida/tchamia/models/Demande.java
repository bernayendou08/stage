package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Demandes")


public class Demande {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String Libele;
    private int Quantiter;
    private String FuullNameRecepteur;
    private Date DateDeLivraison;
    private int AdressDeLivrason;

    @ManyToOne
    @JoinColumn(name = "etat_id")
    private Etat Etat;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Demande() {
    }

    public Demande(String libele, int quantiter, String fuullNameRecepteur, Date dateDeLivraison, int adressDeLivrason, com.musaida.tchamia.models.Etat etat) {
        Libele = libele;
        Quantiter = quantiter;
        FuullNameRecepteur = fuullNameRecepteur;
        DateDeLivraison = dateDeLivraison;
        AdressDeLivrason = adressDeLivrason;
        Etat = etat;
    }

    public Demande(Long id, String libele, int quantiter, String fuullNameRecepteur, Date dateDeLivraison, int adressDeLivrason, com.musaida.tchamia.models.Etat etat) {
        this.id = id;
        Libele = libele;
        Quantiter = quantiter;
        FuullNameRecepteur = fuullNameRecepteur;
        DateDeLivraison = dateDeLivraison;
        AdressDeLivrason = adressDeLivrason;
        Etat = etat;
    }

    public String getLibele() {
        return Libele;
    }

    public void setLibele(String libele) {
        Libele = libele;
    }

    public int getQuantiter() {
        return Quantiter;
    }

    public void setQuantiter(int quantiter) {
        Quantiter = quantiter;
    }

    public String getFuullNameRecepteur() {
        return FuullNameRecepteur;
    }

    public void setFuullNameRecepteur(String fuullNameRecepteur) {
        FuullNameRecepteur = fuullNameRecepteur;
    }

    public Date getDateDeLivraison() {
        return DateDeLivraison;
    }

    public void setDateDeLivraison(Date dateDeLivraison) {
        DateDeLivraison = dateDeLivraison;
    }

    public int getAdressDeLivrason() {
        return AdressDeLivrason;
    }

    public void setAdressDeLivrason(int adressDeLivrason) {
        AdressDeLivrason = adressDeLivrason;
    }

    public com.musaida.tchamia.models.Etat getEtat() {
        return Etat;
    }

    public void setEtat(com.musaida.tchamia.models.Etat etat) {
        Etat = etat;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
