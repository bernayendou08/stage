package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Livraisons")
public class Livraison {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int CodeLivraison;
    private String Longitude;
    private String Latitude;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    @ManyToOne
    @JoinColumn(name = "lot_id")
    private Lot Lot;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User User;

    @ManyToOne
    @JoinColumn(name = "etat_id")
    private Etat Etat;


    public Livraison() {
    }

    public Livraison(int codeLivraison, String longitude, String latitude, Date createdAt, Date updatedAt) {
        CodeLivraison = codeLivraison;
        Longitude = longitude;
        Latitude = latitude;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
    }

    public Livraison(Long id, int codeLivraison, String longitude, String latitude, Date createdAt, Date updatedAt) {
        this.id = id;
        CodeLivraison = codeLivraison;
        Longitude = longitude;
        Latitude = latitude;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCodeLivraison() {
        return CodeLivraison;
    }

    public void setCodeLivraison(int codeLivraison) {
        CodeLivraison = codeLivraison;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
