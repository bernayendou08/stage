package com.musaida.tchamia.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Types")

public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Size;
    private double Prix;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    private String Description;

    public double getPrix() {
        return Prix;
    }

    public void setPrix(double prix) {
        Prix = prix;
    }

    public  String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
