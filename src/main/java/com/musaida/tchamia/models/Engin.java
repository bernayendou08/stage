package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Livraisons")

public class Engin {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int Matricule;
    private String Libele;
    private String TypeEngin;
    private String Description;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    public Engin() {
    }

    public Engin(int matricule, String libele, String typeEngin, String description) {
        Matricule = matricule;
        Libele = libele;
        TypeEngin = typeEngin;
        Description = description;
    }

    public Engin(Long id, int matricule, String libele, String typeEngin, String description) {
        this.id = id;
        Matricule = matricule;
        Libele = libele;
        TypeEngin = typeEngin;
        Description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMatricule() {
        return Matricule;
    }

    public void setMatricule(int matricule) {
        Matricule = matricule;
    }

    public String getLibele() {
        return Libele;
    }

    public void setLibele(String libele) {
        Libele = libele;
    }

    public String getTypeEngin() {
        return TypeEngin;
    }

    public void setTypeEngin(String typeEngin) {
        TypeEngin = typeEngin;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
