package com.musaida.tchamia.models;

import lombok.Data;
import javax.persistence.*;


@Data
@Entity
@Table(name = "Tarifs")
public class Tarif {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String TypeColis;
    private double Montant;

    public Tarif() {
    }

    public Tarif(String typeColis, double montant) {
        TypeColis = typeColis;
        Montant = montant;
    }

    public Tarif(Long id, String typeColis, double montant) {
        this.id = id;
        TypeColis = typeColis;
        Montant = montant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeColis() {
        return TypeColis;
    }

    public void setTypeColis(String typeColis) {
        TypeColis = typeColis;
    }

    public double getMontant() {
        return Montant;
    }

    public void setMontant(double montant) {
        Montant = montant;
    }
}
