package com.musaida.tchamia.models;

public enum ERole {
    ROLE_SUPERADMIN,
    ROLE_ADMIN,
    ROLE_CHEF,
    ROLE_ENTREPRISE,
    ROLE_LIVREUR,
    ROLE_USER,
}
