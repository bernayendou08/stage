package com.musaida.tchamia.models;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Avis")

public class Avis {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int Notation;
    private String Avis;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;

    public Avis() {
    }

    public Avis(int notation, String avis) {
        Notation = notation;
        Avis = avis;
    }

    public Avis(Long id, int notation, String avis) {
        this.id = id;
        Notation = notation;
        Avis = avis;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNotation() {
        return Notation;
    }

    public void setNotation(int notation) {
        Notation = notation;
    }

    public String getAvis() {
        return Avis;
    }

    public void setAvis(String avis) {
        Avis = avis;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
