package com.musaida.tchamia.models;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "TypeLivraisons")

public class TypeService {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String Libele;

    public TypeService() {
    }

    public TypeService(String libele) {
        Libele = libele;
    }

    public TypeService(Long id, String libele) {
        this.id = id;
        Libele = libele;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibele() {
        return Libele;
    }

    public void setLibele(String libele) {
        Libele = libele;
    }
}
