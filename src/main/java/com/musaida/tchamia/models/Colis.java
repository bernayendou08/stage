package com.musaida.tchamia.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Colis")
public class Colis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private int varchar;
    private int Code;
    private String Longitude;
    private String Latitude;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category Category;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User User;

    @ManyToOne
    @JoinColumn(name = "point_id")
    private Point Point;

    @ManyToOne
    @JoinColumn(name = "etat_id")
    private Etat Etat;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type Type;

    @Basic(optional = false)
    @CreationTimestamp
    @Column(name= "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreatedAt;
    @UpdateTimestamp
    @Column(name ="updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date UpdatedAt;


    public Colis() {
    }

    public Colis(int varchar, int code, String longitude, String latitude, Date createdAt, Date updatedAt) {
        this.varchar = varchar;
        Code = code;
        Longitude = longitude;
        Latitude = latitude;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
    }

    public Colis(Long id, int varchar, int code, String longitude, String latitude, Date createdAt, Date updatedAt) {
        this.id = id;
        this.varchar = varchar;
        Code = code;
        Longitude = longitude;
        Latitude = latitude;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
    }

    public int getVarchar() {
        return varchar;
    }

    public void setVarchar(int varchar) {
        this.varchar = varchar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
