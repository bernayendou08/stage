package com.musaida.tchamia.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@Table(name = "geo")
public class Geo{

        private static final long serialVersionUID = 1L;

        @JsonIgnore
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "lat")
        private String lat;

        @Column(name = "lng")
        private String lng;

        @Basic(optional = false)
        @CreationTimestamp
        @Column(name= "createdAt")
        @Temporal(TemporalType.TIMESTAMP)
        private Date CreatedAt;


        @UpdateTimestamp
        @Column(name ="updatedAt")
        @Temporal(TemporalType.TIMESTAMP)
        private Date UpdatedAt;


        public Geo(String lat, String lng) {
            this.lat = lat;
            this.lng = lng;
        }


}
