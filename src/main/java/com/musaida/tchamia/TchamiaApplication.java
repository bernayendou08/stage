package com.musaida.tchamia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TchamiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TchamiaApplication.class, args);
	}

}
