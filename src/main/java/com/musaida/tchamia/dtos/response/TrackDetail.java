package com.musaida.tchamia.dtos.response;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrackDetail {
    @NotNull
    private String Username;
    @NotNull
    private String Number;
    @NotNull
    private String Lat;
    @NotNull
    private String Lng;

}
