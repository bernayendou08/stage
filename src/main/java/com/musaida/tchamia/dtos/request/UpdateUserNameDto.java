package com.musaida.tchamia.dtos.request;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class UpdateUserNameDto {
    @NotNull
    @Size(min=3)
    private String Username;
}
