package com.musaida.tchamia.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class UpdateGeoData {
    @NotNull
    private String Longitude;
    @NotNull
    private String Latitude;

}