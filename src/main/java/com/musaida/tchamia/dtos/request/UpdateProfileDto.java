package com.musaida.tchamia.dtos.request;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProfileDto {
    @NotNull
    private String fullname;

    public static Long getId() {
        return null;
    }
}
