package com.musaida.tchamia.dtos.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdatePasswordDto {
    @NotNull
    @Size(min= 8)
    public String Password;
}
