package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Engin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnginRepository extends JpaRepository<Engin,Long> {
}
