package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Colis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ColisRepository extends JpaRepository<Colis,Long> {
}
