package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Avis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvisRepository extends JpaRepository<Avis,Long> {
}
