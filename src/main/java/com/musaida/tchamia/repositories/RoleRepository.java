package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.ERole;
import com.musaida.tchamia.models.Role;
import com.musaida.tchamia.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

    Optional<Role> findByName(ERole name);

}

