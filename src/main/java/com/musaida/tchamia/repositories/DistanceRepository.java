package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Distance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DistanceRepository extends JpaRepository<Distance, Long> {
}
