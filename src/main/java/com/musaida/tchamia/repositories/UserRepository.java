package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    Boolean existsByNumber(String number);



    @Query("Select u from User u")
    List<User> findAllByRole (Long roleId);

    boolean existsByPassword(String password);
}
