package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Demande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemandeRepository extends JpaRepository<Demande,Long>{
}
