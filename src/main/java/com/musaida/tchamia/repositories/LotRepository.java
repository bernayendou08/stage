package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Lot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LotRepository extends JpaRepository<Lot,Long> {
}
