package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Etat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtatRepository extends JpaRepository<Etat,Long> {
}

