package com.musaida.tchamia.repositories;

import com.musaida.tchamia.models.Tarif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TarifRepository extends JpaRepository<Tarif, Long> {
}
