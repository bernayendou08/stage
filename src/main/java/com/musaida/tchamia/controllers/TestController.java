package com.musaida.tchamia.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

        @GetMapping("/all")
        public String allAccess() {
            return "Public Content.";
        }
        @GetMapping("/user")
        @PreAuthorize("hasRole('USER') or hasRole('SUPERADMIN') or hasRole('ADMIN') or hasRole('CHEF') or hasRole('ENTREPRISE') or hasRole('LIVREUR')  ")
        public String userAccess() {
            return "User Content.";
        }
        @GetMapping("/superadmin")
        @PreAuthorize("hasRole('SUPERADMIN')")
        public String superadminAccess() {
            return "Superadmin Board.";
        }
        @GetMapping("/admin")
        @PreAuthorize("hasRole('ADMIN')")
        public String adminAccess() {
            return "Admin Board.";
        }

        @GetMapping("/chef")
        @PreAuthorize("hasRole('CHEF')")
        public String chefAccess() {
            return "Chef Board.";
        }

        @GetMapping("/entreprise")
        @PreAuthorize("hasRole('ENTREPRISE')")
        public String entrepriseAccess() {
            return "Entreprise Board.";
        }
        @GetMapping("/livreur")
        @PreAuthorize("hasRole('LIVREUR')")
        public String livreurAccess() {
            return "Livreur Board.";
        }


}




