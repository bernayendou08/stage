package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Etat;
import com.musaida.tchamia.services.interfaces.EtatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/etats")

public class EtatController {
    @Autowired
    EtatService etatService;

    @GetMapping
    public ResponseEntity<List<Etat>> getAllEtat(){
        List<Etat> etats = etatService.getAllEtat();
        return new ResponseEntity<>(etats, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Etat> getoneEtat(@PathVariable(name = "id") Long id) {
        Etat etats = etatService.getOneEtat(id);
        return new ResponseEntity<>(etats, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Etat> addoneEtat(@RequestBody @Valid Etat etat){
        Etat etats = etatService.addOneEtat(etat);
        return new ResponseEntity<>(etats, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Etat> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Etat etat){
        Etat etats = etatService.updateEtat(id,etat);
        return new ResponseEntity< >(etats, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteEtat(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = etatService.deleteEtat(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}
