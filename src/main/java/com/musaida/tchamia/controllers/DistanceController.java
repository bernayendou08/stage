package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Distance;
import com.musaida.tchamia.services.interfaces.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/distances")

public class DistanceController {
    @Autowired
    DistanceService distanceService;

    @GetMapping
    public ResponseEntity<List<Distance>> getAlldistance(){
        List<Distance> distances = distanceService.getAllDistance();
        return new ResponseEntity<>(distances, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Distance> getoneDistance(@PathVariable(name = "id") Long id) {
        Distance distances = distanceService.getOneDistance(id);
        return new ResponseEntity<>(distances, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Distance> addoneDistance(@RequestBody @Valid Distance distance){
        Distance distances = distanceService.addOneDistance(distance);
        return new ResponseEntity<>(distances, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Distance> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Distance distance){
        Distance distances = distanceService.updateDistance(id, distance);
        return new ResponseEntity< >(distances, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteDistance(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = distanceService.deleteDistance(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}


