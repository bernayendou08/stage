package com.musaida.tchamia.controllers;


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;

import com.musaida.tchamia.dtos.request.CredentialsDto;
import com.musaida.tchamia.dtos.request.LoginResquest;
import com.musaida.tchamia.dtos.request.SignupResquest;
import com.musaida.tchamia.dtos.request.UpdateUserNameDto;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.MessageResponse;
import com.musaida.tchamia.dtos.response.UserInfoResponse;
import com.musaida.tchamia.exceptions.ResourceNotFoundException;
import com.musaida.tchamia.exceptions.TchamiaException;
import com.musaida.tchamia.models.ERole;
import com.musaida.tchamia.models.Role;
import com.musaida.tchamia.models.User;
import com.musaida.tchamia.repositories.RoleRepository;
import com.musaida.tchamia.repositories.UserRepository;
import com.musaida.tchamia.security.jwt.JwtUtils;
import com.musaida.tchamia.services.UserDetailImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import static com.musaida.tchamia.utils.AppConstant.ID;
import static com.musaida.tchamia.utils.AppConstant.USER;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
        @Autowired
        AuthenticationManager authenticationManager;
        @Autowired
        UserRepository userRepository;
        @Autowired
        RoleRepository roleRepository;
        @Autowired
        PasswordEncoder encoder;
        @Autowired
        JwtUtils jwtUtils;
        @PostMapping("/signin")
        public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginResquest loginRequest) {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetailImpl userDetails = (UserDetailImpl) authentication.getPrincipal();
            ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());
            return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                    .body(new UserInfoResponse(userDetails.getId(),
                            userDetails.getUsername(),
                            userDetails.getEmail(),
                            roles));
        }
        @PostMapping("/signup")
        public ResponseEntity<?> registerUser(@Valid @RequestBody SignupResquest signUpRequest) {
            if (userRepository.existsByUsername(signUpRequest.getUsername())) {
                return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
            }
            if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
            }
            if (userRepository.existsByNumber(signUpRequest.getNumber())){
                return ResponseEntity.badRequest().body(new MessageResponse(("error: Number is already")));
            }
            // Create new user's account
            User user = new User(signUpRequest.getUsername(),
                    signUpRequest.getEmail(), signUpRequest.getFullname(), signUpRequest.getUsername(),
                    encoder.encode(signUpRequest.getPassword()));
            Set<String> strRoles = signUpRequest.getRole();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "superdmin":
                            Role superadminRole = roleRepository.findByName(ERole.ROLE_SUPERADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(superadminRole);
                            break;
                        case "admin":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);
                            break;
                        case "chef":
                            Role chefRole = roleRepository.findByName(ERole.ROLE_CHEF)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(chefRole);
                            break;
                        case "entreprise":
                            Role entrepriseRole = roleRepository.findByName(ERole.ROLE_ENTREPRISE)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(entrepriseRole);
                            break;
                        case "livreur":
                            Role livreurRole = roleRepository.findByName(ERole.ROLE_LIVREUR)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(livreurRole);
                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }
            user.setRoles(roles);
            userRepository.save(user);
            return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
        }
        @PostMapping("/signout")
        public ResponseEntity<?> logoutUser() {
            ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
            return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body(new MessageResponse("You've been signed out!"));
        }

        @PostMapping("/updateusername/{id}")
        public ResponseEntity<ApiResponse> updateUsername(@PathVariable(name = "id") long userId,@Valid @RequestBody  UpdateUserNameDto userNameDto) {
            User user1 = userRepository.findById(userId).orElseThrow(
                    () -> new ResourceNotFoundException(USER,ID,userId ));
            if (userRepository.existsByUsername(userNameDto.getUsername())){
                throw new TchamiaException(HttpStatus.BAD_REQUEST, "username already exist try a new one");
            }
            user1.setUsername(userNameDto.getUsername());
            userRepository.save(user1);

            return new ResponseEntity<>(new ApiResponse(Boolean.TRUE,"username update"), HttpStatus.OK);

        }

        @PostMapping("/updateCredential/{id}")
        public ResponseEntity<ApiResponse> updateCredential(@PathVariable(name = "id")long userId, @Valid @RequestBody CredentialsDto credentialsDto){
            User user2= userRepository.findById(userId).orElseThrow(
                    ()-> new ResourceNotFoundException(USER, ID, userId));
            if (userRepository.existsByUsername(credentialsDto.getUsername())){
                throw new TchamiaException(HttpStatus.BAD_REQUEST, "username already exist try a new one");
            }
            if (userRepository.existsByNumber(credentialsDto.getNumber())){
                throw new TchamiaException(HttpStatus.BAD_REQUEST, "Number already exist try a new one");
            }
            if (userRepository.existsByPassword(credentialsDto.getPassword())){
                throw new TchamiaException(HttpStatus.BAD_REQUEST, "Password already exist try a new one");
            }
                    user2.setNumber(credentialsDto.getNumber());
                    user2.setPassword(credentialsDto.getPassword());
                    user2.setUsername(credentialsDto.getUsername());

            return new ResponseEntity<>(new ApiResponse(Boolean.TRUE,"Credential update"), HttpStatus.OK);


        }

        }
