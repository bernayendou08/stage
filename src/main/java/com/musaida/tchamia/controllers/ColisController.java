package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Colis;
import com.musaida.tchamia.services.interfaces.ColisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


    @RestController
    @RequestMapping("/api/colies")

    public class ColisController {
        @Autowired
        ColisService colisService;

        @GetMapping
        public ResponseEntity<List<Colis>> getAllColis(){
            List<Colis> colies = colisService.getAllColis();
            return new ResponseEntity<>(colies, HttpStatus.OK);
        }
        @GetMapping("/{id}")
        public ResponseEntity<Colis> getoneColis(@PathVariable(name = "id") Long id) {
            Colis colies = colisService.getOneColis(id);
            return new ResponseEntity<>(colies, HttpStatus.OK);
        }
        @PostMapping
        public ResponseEntity<Colis> addoneColis(@RequestBody @Valid Colis colis){
           Colis colies = colisService.addOneColis(colis);
            return new ResponseEntity<>(colies, HttpStatus.CREATED);
        };


        @PutMapping("/{id}")
        public ResponseEntity<Colis> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Colis colis){
            Colis colies = colisService.updateColis(id, colis);
            return new ResponseEntity< >(colies, HttpStatus.OK);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<ApiResponse> deleteColis(@PathVariable(name = "id") Long id) {
            ApiResponse apiResponse = colisService.deleteColis(id);

            return new ResponseEntity< >(apiResponse, HttpStatus.OK);
        }
    }
