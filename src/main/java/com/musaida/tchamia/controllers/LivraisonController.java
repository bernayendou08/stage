package com.musaida.tchamia.controllers;


import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Livraison;
import com.musaida.tchamia.services.interfaces.LivraisonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/livraisons")

public class LivraisonController {
    @Autowired
    LivraisonService livraisonService;

    @GetMapping
    public ResponseEntity<List<Livraison>> getAlllivraison(){
        List<Livraison> livraisons = livraisonService.getAllLivraison();
        return new ResponseEntity<>(livraisons, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Livraison> getonelivraison(@PathVariable(name = "id") Long id) {
        Livraison livraisons = livraisonService.getOneLivraison(id);
        return new ResponseEntity<>(livraisons, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Livraison> addonelivraison(@RequestBody @Valid Livraison livraison){
        Livraison livraisons = livraisonService.addOneLivraison(livraison);
        return new ResponseEntity<>(livraisons, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Livraison> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Livraison livraison){
        Livraison livraisons = livraisonService.updateLivraison(id, livraison);
        return new ResponseEntity< >(livraisons, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletelivraison(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = livraisonService.deleteLivraison(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}
