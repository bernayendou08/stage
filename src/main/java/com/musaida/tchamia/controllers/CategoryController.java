package com.musaida.tchamia.controllers;


import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Category;
import com.musaida.tchamia.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping
   public ResponseEntity<List<Category>> getAllCategory(){
        List<Category> categories = categoryService.getAllCategory();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Category> getoneCategory(@PathVariable(name = "id") Long id) {
        Category categories = categoryService.getOneCategory(id);
              return new ResponseEntity<>(categories, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Category> addoneCategory(@RequestBody @Valid Category category){
        Category categories = categoryService.addOneCategory(category);
               return new ResponseEntity<>(categories, HttpStatus.CREATED);
    };


        @PutMapping("/{id}")
        public ResponseEntity<Category> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Category category){
            Category categories = categoryService.updateCategory(id, category);
            return new ResponseEntity< >(categories, HttpStatus.OK);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<ApiResponse> deletePost(@PathVariable(name = "id") Long id) {
            ApiResponse apiResponse = categoryService.deleteCategory(id);

            return new ResponseEntity< >(apiResponse, HttpStatus.OK);
        }
    }





