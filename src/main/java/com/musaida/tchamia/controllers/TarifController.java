package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Tarif;
import com.musaida.tchamia.services.interfaces.TarifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tarifs")
public class TarifController {

    @Autowired
    TarifService tarifService;

    @GetMapping
    public ResponseEntity<List<Tarif>> getAllTarif(){
        List<Tarif> tarifs = tarifService.getAllTarif();
        return new ResponseEntity<>(tarifs, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Tarif> getonetarif(@PathVariable(name = "id") Long id) {
        Tarif tarifs = tarifService.getOneTarif(id);
        return new ResponseEntity<>(tarifs, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Tarif> addoneTarif(@RequestBody @Valid Tarif tarif){
      Tarif tarifs = tarifService.addOneTarif(tarif);
        return new ResponseEntity<>(tarifs, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Tarif> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Tarif tarif){
        Tarif tarifs = tarifService.updateTarif(id, tarif);
        return new ResponseEntity< >(tarifs, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletetarif(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = tarifService.deleteTarif(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}
