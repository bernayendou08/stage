package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Avis;
import com.musaida.tchamia.services.interfaces.AvisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/avis")

public class AvisController {
    @Autowired
    AvisService avisService;

    @GetMapping
    public ResponseEntity<List<Avis>> getAllAvis(){
        List<Avis> avies = avisService.getAllAvis();
        return new ResponseEntity<>(avies, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Avis> getoneAvis(@PathVariable(name = "id") Long id) {
      Avis avies = avisService.getOneAvis(id);
        return new ResponseEntity<>(avies, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Avis> addoneAvis(@RequestBody @Valid Avis avis){
        Avis avies = avisService.addOneAvis(avis);
        return new ResponseEntity<>(avies, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Avis> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Avis avis){
        Avis avies = avisService.updateAvis(id, avis);
        return new ResponseEntity< >(avies, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteavis(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = avisService.deleteAvis(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}
