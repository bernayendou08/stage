package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Demande;
import com.musaida.tchamia.services.interfaces.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/demandes")
public class DemandeController {
    @Autowired
    DemandeService demandeService;

    @GetMapping
    public ResponseEntity<List<Demande>> getAllColis(){
        List<Demande> demandes = demandeService.getAllDemande();
        return new ResponseEntity<>(demandes, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Demande> getoneDemande(@PathVariable(name = "id") Long id) {
       Demande demandes = demandeService.getOneDemande(id);
        return new ResponseEntity<>(demandes, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Demande> addoneDemande(@RequestBody @Valid Demande demande){
        Demande demandes = demandeService.addOneDemande(demande);
        return new ResponseEntity<>(demandes, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Demande> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Demande demande){
        Demande demandes = demandeService.updateDemande(id, demande);
        return new ResponseEntity< >(demandes, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteDemande(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = demandeService.deleteDemande(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}

