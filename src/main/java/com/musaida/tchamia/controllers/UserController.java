package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.request.SignupResquest;
import com.musaida.tchamia.dtos.request.UpdateGeoData;
import com.musaida.tchamia.dtos.request.UpdatePasswordDto;
import com.musaida.tchamia.dtos.request.UpdateProfileDto;
import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.dtos.response.MessageResponse;
import com.musaida.tchamia.models.ERole;
import com.musaida.tchamia.models.Role;
import com.musaida.tchamia.models.User;
import com.musaida.tchamia.repositories.RoleRepository;
import com.musaida.tchamia.repositories.UserRepository;
import com.musaida.tchamia.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;



    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupResquest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }
        if (userRepository.existsByNumber(signUpRequest.getNumber())){
            return ResponseEntity.badRequest().body(new MessageResponse(("error: Number is already")));
        }
        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getFullname(), signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()));
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "superdmin":
                        Role superadminRole = roleRepository.findByName(ERole.ROLE_SUPERADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(superadminRole);
                        break;
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                        break;
                    case "chef":
                        Role chefRole = roleRepository.findByName(ERole.ROLE_CHEF)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(chefRole);
                        break;
                    case "entreprise":
                        Role entrepriseRole = roleRepository.findByName(ERole.ROLE_ENTREPRISE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(entrepriseRole);
                        break;
                    case "livreur":
                        Role livreurRole = roleRepository.findByName(ERole.ROLE_LIVREUR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(livreurRole);
                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        userRepository.save(user);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PostMapping("/Profil/Update/{id}")
    public ResponseEntity<ApiResponse> UpdateUserProfil(@PathVariable(name="id") Long userId,@Valid @RequestBody UpdateProfileDto updateProfileDto){
        ApiResponse apiResponse = userService.updateUserProfil(userId, updateProfileDto);
    return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/Password/forgot/{id}")
    public ResponseEntity<ApiResponse>forgotPassword(@PathVariable(value = "email") String email){
        ApiResponse apiResponse= userService.forgotPassword(email);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PostMapping("/password/update/{id}")
    public ResponseEntity<ApiResponse> updatePassword(@PathVariable(name="id") Long userId, UpdatePasswordDto updatePasswordDto){
        ApiResponse apiResponses = userService.updatePassword(userId, updatePasswordDto.Password);
        return new ResponseEntity<>(apiResponses,HttpStatus.OK);
    }

    @GetMapping("/User/All/{id}")
    public ResponseEntity<List<User>>getAllUserInDB(){
        List<User> user= userService.getAllUserInDB();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/User/Update/{id}")
    public ResponseEntity<ApiResponse>updateUserSate(@PathVariable(name="id") long userId){
        ApiResponse apiResponse= userService.updateUserSate(userId);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/Geo/Update/{id}")
    public ResponseEntity<ApiResponse>updateGeoData(@PathVariable(name="id") long userId, @Valid @RequestBody UpdateGeoData updateGeoData){
        ApiResponse apiResponse= userService.updateGeoData(userId, updateGeoData);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}


