package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Lot;
import com.musaida.tchamia.services.interfaces.LotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lots")
public class LotController {

    @Autowired
    LotService lotService;

    @GetMapping
    public ResponseEntity<List<Lot>> getAllLot(){
        List<Lot> lots = lotService.getAllLot();
        return new ResponseEntity<>(lots, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Lot> getonelots(@PathVariable(name = "id") Long id) {
        Lot lots = lotService.getOneLot(id);
        return new ResponseEntity<>(lots, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Lot> addonelot(@RequestBody @Valid Lot lot){
        Lot lots = lotService.addOneLot(lot);
        return new ResponseEntity<>(lots, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Lot> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Lot lot){
        Lot lots = lotService.updateLot(id, lot);
        return new ResponseEntity< >(lots, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletelot(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = lotService.deleteLot(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }

}
