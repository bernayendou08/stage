package com.musaida.tchamia.controllers;


import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Point;
import com.musaida.tchamia.services.interfaces.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/points")
public class PointController {
    @Autowired
    PointService pointService;

    @GetMapping
    public ResponseEntity<List<Point>> getAllPoint(){
        List<Point> points = pointService.getAllPoint();
        return new ResponseEntity<>(points, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Point> getonePoint(@PathVariable(name = "id") Long id) {
        Point points = pointService.getOnePoint(id);
        return new ResponseEntity<>(points, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Point> addonePoint(@RequestBody @Valid Point point){
        Point points = pointService.addOnePoint(point);
        return new ResponseEntity<>(points, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Point> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Point point){
        Point points = pointService.updatePoint(id, point);
        return new ResponseEntity< >(points, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletePoint(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = pointService.deletePoint(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}