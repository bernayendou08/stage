package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Payement;
import com.musaida.tchamia.services.interfaces.PayementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/payements")

public class PayementController {
    @Autowired
    PayementService payementService;

    @GetMapping
    public ResponseEntity<List<Payement>> getAllpayement(){
        List<Payement> payements = payementService.getAllPayements();
        return new ResponseEntity<>(payements, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Payement> getonePayement(@PathVariable(name = "id") Long id) {
        Payement payements = payementService.getOnePayement(id);
        return new ResponseEntity<>(payements, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Payement> addonepayement(@RequestBody @Valid Payement payement){
        Payement payements = payementService.addOnePayement(payement);
        return new ResponseEntity<>(payements, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Payement> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Payement payement){
        Payement payements = payementService.updatePayement(id, payement);
        return new ResponseEntity< >(payements, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletepayement(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = payementService.deletePayement(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }

}

