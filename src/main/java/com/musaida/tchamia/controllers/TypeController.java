package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Type;
import com.musaida.tchamia.services.interfaces.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/types")

public class TypeController {
    @Autowired
    TypeService typeService;

    @GetMapping
    public ResponseEntity<List<Type>> getAlltype(){
        List<Type> types = typeService.getAllType();
        return new ResponseEntity<>(types, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Type> getonetype(@PathVariable(name = "id") Long id) {
        Type types = typeService.getOneType(id);
        return new ResponseEntity<>(types, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Type> addonetype(@RequestBody @Valid Type type){
        Type types = typeService.addOneType(type);
        return new ResponseEntity<>(types, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Type> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Type type){
        Type types = typeService.updateType(id, type);
        return new ResponseEntity< >(types, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletetype(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = typeService.deleteType(id);
        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }


    }
