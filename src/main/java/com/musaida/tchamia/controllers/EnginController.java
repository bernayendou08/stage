package com.musaida.tchamia.controllers;

import com.musaida.tchamia.dtos.response.ApiResponse;
import com.musaida.tchamia.models.Colis;
import com.musaida.tchamia.models.Engin;
import com.musaida.tchamia.services.interfaces.EnginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/engins")

public class EnginController {
    @Autowired
    EnginService enginService;

    @GetMapping
    public ResponseEntity<List<Engin>> getAllEngin(){
        List<Engin> engins = enginService.getAllEngin();
        return new ResponseEntity<>(engins, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Engin> getoneEngin(@PathVariable(name = "id") Long id) {
        Engin engins = enginService.getOneEngin(id);
        return new ResponseEntity<>(engins, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Engin> addoneEngin(@RequestBody @Valid Engin engin){
        Engin engins = enginService.addOneEngin(engin);
        return new ResponseEntity<>(engins, HttpStatus.CREATED);
    };


    @PutMapping("/{id}")
    public ResponseEntity<Engin> update(@PathVariable(name = "id") Long id, @RequestBody @Valid Engin engin){
        Engin engins = enginService.updateEngin(id, engin);
        return new ResponseEntity< >(engins, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteEngin(@PathVariable(name = "id") Long id) {
        ApiResponse apiResponse = enginService.deleteEngin(id);

        return new ResponseEntity< >(apiResponse, HttpStatus.OK);
    }
}
