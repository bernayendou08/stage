package com.musaida.tchamia.exceptions;

import org.springframework.http.HttpStatus;

public class TchamiaException extends RuntimeException{

    private static final Long serialVersionUID = -6593330219878485669L;
    private final HttpStatus status;
    private final String message;


    public TchamiaException(HttpStatus status, String message) {
        super();
        this.status = status;
        this.message = message;
    }
    public TchamiaException(HttpStatus status, String message, Throwable exception) {
        super(exception);
        this.status = status;
        this.message = message;
}

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
