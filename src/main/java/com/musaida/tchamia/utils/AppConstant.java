package com.musaida.tchamia.utils;

public class AppConstant {
    public static final String CATEGORY = "Category";
    public static final String ID = "Id";

    public static final String TYPE ="Type";

    public static final String ROLE ="Role";

    public static final String ETAT = "Etat";

    public static final String COLIS = "Colis";
    public static final String DEMANDE ="Demande";
    public static final String LIVRAISON = "Livraison";
    public static final String LOT = "LOT";
    public static final String AVIS = "Avis";
    public static final String ENGIN ="Engin";
    public static final String TYPESERVICE = "TypeService";
    public static final String PAYEMENT =  "Payement";
    public static final String TARIF = "Tarif";
    public static final String DISTANCE = "Distance";
    public static final String POINT ="point";
    public static final String USER = "User";
    public static final String EMAIL = "Email";
}
